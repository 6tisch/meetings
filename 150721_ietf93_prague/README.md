| status            | presenter                 | filename                                             |
|-------------------|---------------------------|------------------------------------------------------|
| OK                | Chairs                    | `ietf93_00_intro-status.ppt`                         |
| OK                | Miguel Angel Reina Ortega | `ietf93_01_6tisch_plugtests_report.ppt`              |
| OK                | Dominique Barthel         | `ietf93_02_draft-munoz-6tisch-minimal-examples.ppt`  |
| OK                | Thomas Watteyne           | `ietf93_03_hackathon_report.ppt`                     |
| OK                | Pascal Thubert            | `ietf93_04_draft-ietf-6tisch-architecture-08.ppt`    |
| **MISSING!!**     | Gabriel Montenegro        | `ietf93_05_6lorh.ppt`                                |
| OK                | Xavi Vilajosana           | `ietf93_06_draft-ietf-6tisch-minimal-10.ppt`         |
| OK                | Pascal Thubert            | `ietf93_07_sheperd_status.ppt`                       |
| OK                | Xavi Vilajosana           | `ietf93_08_draft-ietf-6tisch-6top-interface-04.ppt`  |
| OK                | Peter van der Stok        | `ietf93_09_draft-vanderstok-core-comi-patch.ppt`     |
| OK                | Michel Veillette          | `ietf93_10_cool.ppt`                                 |
| OK                | Diego Dujovne             | `ietf93_11_draft-dujovne-6tisch-on-the-fly-06.ppt`   |
| OK                | Qin Wang                  | `ietf93_12_draft-wang-6tisch-6top-coapie-01.ppt`     |
| OK                | Lou Berger                | `ietf93_13_detnet_BoF_news.ppt`                             |
| OK                | Chonggang Wang            | `ietf93_14_draft-wang-6tisch-track-use-cases-01.ppt` |
| OK                | Pascal Thubert            | `ietf93_15_draft-thubert-6tisch-4detnet-01.ppt`      |
| **MISSING!!**     | Chairs                    | `ietf93_16_conclusions.ppt`                          |
