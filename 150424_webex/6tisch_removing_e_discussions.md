This file has been prepared by Thomas Watteyne on April 24 2015 in preparation
for the 6TiSCH interim meeting. It summarizes the WG discussions about the
impact of switching to IEEE802.15.4-2015 for 6TiSCH, closely related to the
proposal known as "drop the \"e\"".

Context
=======

Discussion started in Dallas, see http://www.ietf.org/proceedings/92/slides/slides-92-6tisch-2.pdf:

* [78] bullet 1: remove "e" from charter
* [80+] changes between IEEE802.15.4e-2012 and IEEE802.15.4-2015
    * default value of macTsRxOffset (1120us -> 1116us)
    * changes in length of address used for nonce calculation (2-byte short addresses not allowed anymore)
    * IE space renumbering
* 3 options:
    * use IEEE802.15.4e-2012
    * use IEEE802.15.4-2015
    * use IEEE802.15.4

Questions
=========

* Are we talking about the I-Ds or just the charter?
* different people answer different questions
    * should we reference IEEE802.15.4e-2012 or IEEE802.15.4-2015?
    * should we reference IEEE802.15.4e-2012 or IEEE802.15.4?
* some discussion about the 6TiSCH IG at IEEE802.15, which is off-topic here

ML activity
===========

-----

* position: **AGAINST**
* Date: Mon, 30 Mar 2015 10:12:00 -0700
* From: Kris Pister <ksjp at berkeley.edu>
* Full e-mail: http://www.ietf.org/mail-archive/web/6tisch/current/msg03117.html

> If the roll-up from 15.4e to 15.4-2015 were just corrections of real problems
> it would be a no-brainer.
> Unfortunately, there are numerous cases where IEEE members with "good ideas"
> have pushed for changes that have little or no practical value, but do require a re-write of existing code, and mandate non-interoperability between the existing
> 15.4e and the forthcoming 15.4-2015.

-----

* position: **AGAINST**
* Date: Thu, 23 Apr 2015 10:59:34 -0400
* From: Michael Richardson <mcr+ietf at sandelman.ca>
* Full e-mail: http://www.ietf.org/mail-archive/web/6tisch/current/msg03292.html

> I don't have a strong opinion, and I feel that some of the layer-9
> (political) layer issues are beyond me.  What I heard suggests that there are
> actual technical things that might force us to retain the earlier year
> reference, and that there are technical reasons to want to remove the "e",
> and the year reference.
> 
> It seems therefore that the right thing is to leave the "e" in place for
> this "volume" of the architecture, to let the IEEE publish the -2015
> document, and then consider whether we can/should reference 802.15.4-2015,
> or 802.15.4(-noyear) for volume 2.
> 
> Perhaps it is enough to say, "we believe that the 6tisch requirements are
> consistent with the not-yet-published 802.15.4-2015 document"

-----

* position: **AGAINST**
* Date: Mon, 30 Mar 2015 14:22:15 -0400
* From: Rene Struik <rstruik.ext at gmail.com>
* Full e-mail: http://www.ietf.org/mail-archive/web/6tisch/current/msg03118.html

> I think the proper approach is that one investigates whether an updated version of a cross-referenced specification is suitable and, if so, one can adopt this.
> 
> To me, it is not a given that the revision effort of 802.15.4 that is currently on its way will automatically satisfy requirements 6TiSCH has. I am wondering how we would know, since so far technical changes have never been discussed/socialized within 6TiSCH itself (see also http://www.ietf.org/mail-archive/web/6tisch/current/msg02853.html). Anecdotal evidence regarding extensive changes makes me somewhat pessimistic here.
> 
> We should use a "trust but verify" approach here. Anything else would be imprudent. I think this more or less reiterates Subir Das's point.
> 
> So, I am not in favor of implicit, blind trust; only in favor of explicit, verified trust. Unfortunately, this means I think the recommendation you put forward below is imprudent.
> 
> As a final note:
> a) not sure why the 802.15.4e amendment would be incomplete. This seems to be in direct conflict with the note on page 1 of the 802.15.4e-2012 standard (NOTE—The editing instructions contained in this amendment define how to merge the material contained therein into the existing base standard and its amendments to form the comprehensive standard).
> b) Not sure how one can already predict now when the current revision work will be end and result in a revised standard. Assuming that this will be in 2015 (with sponsor ballot not having started yet) seems somewhat premature.

-----

* position: **AGAINST**
* Date: Mon, 30 Mar 2015 18:26:35 +0000
* From: "Turner, Randy" <Randy.Turner at landisgyr.com>
* Full e-mail: http://www.ietf.org/mail-archive/web/6tisch/current/msg03119.html

> If Kris is correct, and compliance with the 15.4 rollup would cause code changes to existing 4e code, then I don't think it's a good idea to remove the 'e' -- if existing 4e code would be interoperable with 15.4 rollup, then I guess referencing the rollup (for posterity) would be a good idea.

-----

* position: **IN_FAVOR**
* Date: Mon, 30 Mar 2015 14:06:36 -0500
* From: Pat Kinney <pat.kinney at kinneyconsultingllc.com>
* Full e-mail: http://www.ietf.org/mail-archive/web/6tisch/current/msg03120.html

> I disagree with both Kris' and Rene's objections.
> 
> Addressing Kris' objections first:
> - If the roll-up from 15.4e to  15.4-2015 were just corrections of real problems it would be a no-brainer.
> <PK> the element here is "just corrections", indeed there are corrections, but many non-TSCH and security corrections have been made as well
> - Unfortunately, there are numerous cases where IEEE members with "good ideas" have pushed for changes that have little or no practical value, but do require a re-write of existing code, and mandate non-interoperability between the existing 15.4e and the forthcoming 15.4-2015.
> <PK> '"good ideas" with little or no practical value' is a personal opinion, to which I disagree whole heartedly.'Requiring a rewrite of existing code and mandating non-interoperability between existing 15.4e and forthcoming 15.4-2015' is unfounded, the changes we have implemented have been critiqued for backward compatibility, I am not aware of any change that would make the two versions of devices non-interoperable. I ask that Kris quantify his statement with evidence.
> 
> Addressing Rene's objections:
> - To me, it is not a given that the revision effort of 802.15.4 that is currently on its way will automatically satisfy requirements 6TiSCH has. 
> <PK> to what TSCH requirements does Rene refer? I ask that Rene to not critique without giving evidence
> - I am wondering how we would know, since so far technical changes have never been discussed/socialized within 6TiSCH itself (see also http://www.ietf.org/mail-archive/web/6tisch/current/msg02853.html). Anecdotal evidence regarding extensive changes makes me somewhat pessimistic here.
> <PK> the url provided by Rene merely restates his objections in the past, they absolutely have no evidence supporting them, e.g. "this does not necessarily mean they can speak for 6tisch's interests, at least not without consensus." I ask Rene again, what 6tisch interests with a consensus have been ignored or disregarded? Please stop referring to past innuendos but instead let's discuss specific technical questions
> - So, I am not in favor of implicit, blind trust; only in favor of explicit, verified trust. Unfortunately, this means I think the recommendation you put forward below is imprudent.
> <PK> I addressed the aspect of "the explicit verified trust" at the meeting in Dallas, the latest draft revision will be made publicly available at the start of the Sponsor Ballot which could start as early as 8 April 2015. Until then, I ask that anyone in the 6tisch group to address specific questions to this ML to which I can specifically reply.

-----

* position: **AGAINST**
* Date: Mon, 30 Mar 2015 15:53:30 -0400
* From: Rene Struik <rstruik.ext at gmail.com>
* Full e-mail: http://www.ietf.org/mail-archive/web/6tisch/current/msg03121.html

> I do not understand how one could possibly object against any suggestion that using blind, unverified trust (aka "blank cheque") as yardstick is imprudent: this should be self-evident.
> 
> In fact, my simple suggestion that 6TiSCH do its own due diligence as to whether a specific specification should be preferred as baseline is not unlike what is suggested in the reference sections of most IEEE documents (where one either cross-references dated or undated specs). I
> 
> Ironically, I was told that the latest 802.15.4 Revision draft (the one you had some slides on during IETF-92) does the same... (see below)
> 
> [802.15.4-2006, Section 2 - Normative References]
> The following referenced documents are indispensable for the application of this document. For dated references, only the edition cited applies. For undated references, the latest edition of the referenced document (including any amendments or corrigenda) applies.
> 
> [draft 802.15.4 REVc - DF4]
> The following referenced documents are indispensable for the application of this document (i.e., they must be understood and used, so each referenced document is cited in text and its relationship to this document is explained). For dated references, only the edition cited applies. For undated references, the latest edition of the referenced document (including any amendments or corrigenda) applies.

-----

* position: **AGAINST**
* Date: Mon, 30 Mar 2015 14:56:39 -0700
* From: Tom Phinney <tom.phinney at cox.net>
* Full e-mail: http://www.ietf.org/mail-archive/web/6tisch/current/msg03122.html

> I was contributing a comment on this subject via the chat channel when the Thursday meeting closed. Diego, who was managing the chat channel, captured it and we subsequently have had the attached exchange.
> 
> In ISO and IEC standards the practice is to list the generic document within the list of Normative References unless there are explicit references to specific subclauses of the referenced document, in which case a specific edition needs to be called out because subclause numbering often change from one edition to the next.
> 
> Thus, if a 6tisch document references subclause m.n.p within the new 2015 edition of IEEE802.15.4, the reference would be to IEEE802.15.4:2015, m.n.p. Otherwise the reference WITHIN the standard is simply to IEEE802.15.4, without qualification as to the specific edition. While such an unqualified reference presumes that material on the referenced topic will not disappear from future editions of IEEE802.15.4, such an assumption is almost always valid and leads to much less confusion by those trying to use the standard over its lifetime.
> 
> The specific wording to cover both dated and undated references that the latest version of the ISO/IEC editing directives itself uses is:
> The following documents, in whole or in part, are normatively referenced in this document and
> are indispensable for its application. For dated references, only the edition cited applies. For
> undated references, the latest edition of the referenced document (including any
> amendments) applies.
> 
> The related text from the ISO/IEC editing directives with regard to use of Normative References is:
> 6.2.2 Normative references
> 
> This conditional element shall give a list of the referenced documents cited (see 6.6.7.5) in
> the document in such a way as to make them indispensable for the application of the
> document. For dated references, each shall be given with its year of publication, or, in the
> case of enquiry or final drafts, with a dash together with a footnote “To be published.”, and full
> title. The year of publication or dash shall not be given for undated references. When an
> undated reference is to all parts of a document, the publication number shall be followed by
> the indication “(all parts)” and the general title of the series of parts (i.e. the introductory and
> main elements, see Annex E).
> 
> In principle, the referenced documents shall be documents published by ISO and/or IEC.
> Documents published by other bodies may be referred to in a normative manner provided that
> a) the referenced document is recognized by the ISO and/or IEC committee concerned as
> having wide acceptance and authoritative status as well as being publicly available,
> b) the ISO and/or IEC committee concerned has obtained the agreement of the authors or
> publishers (where known) of the referenced document to its inclusion and to its being
> made available as required — the authors or publishers will be expected to make
> available such documents on request,
> c) the authors or publishers (where known) have also agreed to inform the ISO and/or IEC
> committee concerned of their intention to revise the referenced document and of the
> points the revision will concern, and
> d) the ISO and/or IEC committee concerned undertakes to review the situation in the light of
> any changes in the referenced document.
> 
> The list shall be introduced by the following wording:
> “The following documents, in whole or in part, are normatively referenced in this
> document and are indispensable for its application. For dated references, only the edition
> cited applies. For undated references, the latest edition of the referenced document
> (including any amendments) applies.”
> 
> The above wording is also applicable to a part of a multipart document.
> 
> The list shall not include the following:
> • referenced documents which are not publicly available;
> • referenced documents which are only cited in an informative manner;
> • referenced documents which have merely served as bibliographic or background material
> in the preparation of the document.
> 
> Such referenced documents may be listed in a bibliography (see 6.4.2).
> 
> An example from the same ISO/IEC Directives, Part 2 document demonstrates proper dated-citation usage, including usage of the date only as necessary to reduce ambiguity:
> D.1.1 Scope of rules and examples provided in Annex D
> 
> Annex D provides a synthesis of the rules and examples given in ISO 10241-1:2011, and is
> intended to cover those rules applicable to the forms of terms and definitions most commonly
> present in ISO and IEC standards. For the complete set of rules and examples, refer to
> ISO 10241-1.
> 
> 
> 
> It seems likely that actual references to the 2015 edition of IEEE802.15.4 will, in fact, need to cite specific subclauses. If that is the case, then this entire discussion of whether or not to cite the edition is moot, because such citations are essential to ensure that any specific sublclause references actually point to relevant text.

-----

* position: **AGAINST**
* Date: Mon, 30 Mar 2015 21:44:48 -0400
* From: Robert Moskowitz <rgm-ietf at htt-consult.com>
* Full e-mail: http://www.ietf.org/mail-archive/web/6tisch/current/msg03123.html

> I will try to add me opinion here.
> 
> I am strongly in favor, that at this point to change the reference to IEEE 802.15.4-2015 for IEEE 802.15.4-2011 plus 802.15.4e.
> 
> I am ASSuMEing that you are referencing 802.15.4-2011 and not -2006? What is incomplete about 15.4e, is that you need to reference 15.4-2011.
> 
> But when we were plowing through the security portion of 15.4-2011 to work out what 802.15.9 would be 'controlling', we learned many vendors were still implementing the -2006 security, not the -2011, which was considered more broken that the -2006. Most noteably the 15.4g vendors were making these statements.
> 
> So what went on at the 802.15.4 maintenance sessions? 'Oh, that is REALLY broken. How did we miss that?' A lot of attention was given to not breaking deployed code, but in a few places, broken things needed to be fixed. Tero (and I to some extent) really worked over the security section. Rekeying a PAN really did not work with the way the frame counter was described, for example. We had to move some fields around in the security tables.
> 
> Review? Well get on the sponsor ballot, if you still can. Changes for the sake of changing or for a particular vendor's 'needs'? I did not see that happening. But I am NOT the expert on most of the features, and someone could easily slip that by me.
> 
> Restructuring, a little bit, the Information Elements to allow for SDO requests; most vocal being ETSI.
> 
> I strongly recommend moving forward with referencing 802.15.4-2015.

-----

* position: **AGAINST**
* Date: Mon, 30 Mar 2015 19:59:49 -0700
* From: Kris Pister <ksjp at berkeley.edu>
* Full e-mail: http://www.ietf.org/mail-archive/web/6tisch/current/msg03124.html

> Pat - I'm going by what you presented at IETF91 a few months ago:
> http://www.ietf.org/proceedings/91/slides/slides-91-6tisch-8.pdf
> * IE regions are moving. 0x00-0x19 were unmanaged, now 0x01-0x18 are reserved
> * macTsRxOffset change from 1120 to 1020us
> * not allowed to use short addresses in nonce generation
> Smart people are making arguments on both sides of these issues. I would argue that this means that these are not corrections, they are changes to an existing standard without clear technical consensus. The short address change is absolutely
> a backward compatibility killer.
> 
> In addition, we had a long fight over changing IE format from {Length, Type, Value} to {Type, Length, Value}. The last I heard we lost that battle. That is absolutely a backward compatibility killer. Perhaps that one did finally get resolved in favor of 4e, but the fact that such a change, with little to no practical value, would require continuous debate over many meetings is a sign that backward compatibility is
> not top of mind for a good part of the WG.  That's a real concern.

-----

* position: **IN_FAVOR**
* Date: Tue, 31 Mar 2015 01:19:43 -0500
* From: Pat Kinney <pat.kinney at kinneyconsultingllc.com>
* Full e-mail: http://www.ietf.org/mail-archive/web/6tisch/current/msg03125.html

> Thanks Kris;
> 
> Let's review your points:
> 
> - IE IDs:  When 15.4e was introduced, we allowed vendors to freely use 25 non-standardized IDs with the explicit warning that another vendor could be using that ID creating a conflict, destroying coexistence. Since that time we have seen a significant number of requests for IE IDs, so given the limited number of IDs and the fact that there really is no vendor coexistence with the original scheme, we changed to one specific vendor ID where the vendor's OUI is required to make the IE unique.
> 
> - macTsRxOffset: the basic intent of macTsRxOffset is to center the transmission start within the range of 2200 µs (macTSRxWait). So the transmitter's macTsTxOffset (2120µs) should be 1100µs (macTsRxWait/2) greater than macTxRxOffset(1020µs).  However, 15.4e mistakenly used the term "frame" when it actually meant "PPDU". The start of the frame occurs 192 µs after the start of the PPDU or packet. 15.4e's value of 1120µs was an extra 100µs to compensate for the PHY components (which are really 192 µs). In summary, we could either continue to use the term frame with macTxRxOffset=1020µs, or use PPDU and add 192 µs.
> 
> - use of short addresses in nonce generation: 15.4e changed the nonce requirement of 15.4-2011, from requiring the use of the 8-octet extended address to allowing the usage of the short address (btw - all operations modes not just TSCH). It is generally understood that using the short address in the nonce generation is very problematic. Even if the short address is unique inside the PAN, there could be multiple PANs sharing the same keying material and using same short address for multiple devices. Using only the short address and not a combination of PANId and macShortAddress since it also puts another requirement, that short address needs to be unique over all PANs which use same key, not only unique inside the current PAN.
> 
> - TLV vs. LTV: The revision retains the IE formatting from 15.4e, it remains backward compatible.  The statement "the fact that such a change, with little to no practical value, would require continuous debate over many meetings is a sign that backward compatibility is not top of mind for a good part of the WG" is totally incorrect.  All standards organizations require that all individuals and organizations be heard, and they were in a thoughtful manner.
> 
> In summary, the changes done in the revision were to correct issues and problems.  The standardized TSCH IE IDs were not changed, rather a method was added to eliminate vendor conflicts in non-standardized IDs and the extra IDs were put back into reserve.  The macTxRxOffset was changed to be consistent with the correct terminology. While using the short address for nonce generation is not compliant to the revision, the group consensus was that it's not appropriate to knowingly degrade security, hence reverting back to the 802.15.4-2011 method.

-----

* position: **DISCUSSION**
* Date: * Date: Tue, 31 Mar 2015 06:49:19 +0000
* From: "Pascal Thubert (pthubert)" <pthubert at cisco.com>
* Full e-mail: http://www.ietf.org/mail-archive/web/6tisch/current/msg03126.html

> I do agree that Kris is right, but probably not on the same point as you do here.  6TiSCH does not have much choice. We do not own 802.15.4, we reference it.
> And the question of backward compatibility does not really affect 6TiSCH since 6TiSCH never shipped before, and there certainly will be quite a development effort to build a 6TiSCH product anyway.
> 
> 6TiSCH cannot be locked referencing an interim specification, so there is no question in my mind that 6TiSCH will ultimately refer to the new standard when it ships. What we're after is the minimum hassle on that path.
> 
> The question that was asked to the group is about the current charter, and the current WG docs that we are shipping to IESG, in particular minimal.
> If we keep referencing 15.4e, we will be creating a backward compatibility issue with ourselves, and that's probably not a good idea. I'm looking for a consensus to avoid that.
> 
> Another aspect of that question is the plug test in Prague. Considering the time frame, it will be difficult for the implementations to migrate to 802.15.4-2015, and there may be interop problems at the MAC layer.
> What should we be shooting for?

-----

* position: **DISCUSSION**
* Date: Tue, 31 Mar 2015 07:07:19 +0000
* From: "Pascal Thubert (pthubert)" <pthubert at cisco.com>
* Full e-mail: http://www.ietf.org/mail-archive/web/6tisch/current/msg03128.html

> So far, 6TiSCH has not expressed requirements on TSCH. We have taken it as a base and built upon it. So I can hardly relate your words to something specific. 
> 
> - The base that we have considered for the architecture is very abstract since we had a goal to design beyond 802.15.4 whenever possible. From Pat's presentation, I have trouble believing that this text is affected at all, but then I'm interested in any specific point, and it is still time to fix Archie's text.
> 
> - For the more specific draft, a good suggestion could be to ask the Interest Group to review the minimal and 6top interface drafts and assert whether and how they are affected by the change to 802.15.4-2015, so as to promote an action either on the IEEE or on the IETF side. I gathered from Pat that they are not, but a formal review from the I-G could help close that discussion.
> 
> Pat, do you think that can be done?

-----

* position: **DISCUSSION**
* Date: Tue, 31 Mar 2015 07:30:06 +0000
* From: * From: "Turner, Randy" <Randy.Turner at landisgyr.com>
* Full e-mail: http://www.ietf.org/mail-archive/web/6tisch/current/msg03130.html

> I agree with you in principal about the timing of 6tisch completion and
> the availability of the final 802.15.4 rollup - I think they will be timed
> pretty close so it would be nice to reference the rollup.
> 
> If by ³interim² you mean ³amendment², I believe there are quite a few stds
> that refer to amendments with regards to requirements - 6tisch never
> shipped before but there¹s quite a bit of code already written to 4e, not
> only functional code, but diagnostic (sniffer) code as well, so if there
> are changes required for the rollup, it would be unfortunate.
> 
> Ultimately I think we¹re bound by our charter, not any philosophical
> thinking that we would ³like² to exist that says we do this or that -
> philosophically I think it makes sense to refer to the rollup - it just
> seems like the thing to do. However, we¹re bound by the charter that says
> ³The working group will focus on enabling IPv6 over the TSCH mode of
> 802.15.4e standard².
> 
> I would like to be able to refer to 2015 802.15.4 but frankly the news
> about it being different than 4e (with regards to TSCH) surprised me.
> 
> Could someone elaborate on the deltas between 4e and 2015-802.15.4 for
> TSCH ?
> 
> I guess our AD could ³green light² the reference either way, and like I
> said, it would be nice to refer to the rollup ³for posterity²; meaning,
> for future developers it would be nice if they could work with the more
> current spec - I¹m in favor of the rollup if the differences I am hearing
> about between 4e and the rollup are minimal.  If the changes are
> significant, then from an IETF procedural standpoint, I think we would
> need a ruling.

-----

* position: **AGAINST**
* Date: Tue, 31 Mar 2015 10:57:17 +0100
* From: Robert Cragie <robert.cragie at gridmerge.com>
* Full e-mail: http://www.ietf.org/mail-archive/web/6tisch/current/msg03131.html

> I agree with Rene that there is anecdotal evidence of extensive changes made when they were really unnecessary. This occurred in 802.15.4-2011. I also share Kris' concerns that there will be "changes that have little or not but do require a re-write of existing code, and mandate non-interoperability between the existing 15.4e and the forthcoming 15.4-2015".
> 
> I would hope the wrap-up is comprised solely of 802.15.4-2011 plus the amendments and that there is no wholesale "tweaking" of the text in the misguided view of a handful of individuals that there is something amiss with the current text that needs to be changed.

-----

* position: **AGAINST**
* Date: * Date: Tue, 31 Mar 2015 08:21:53 -0400
* From: Brian Haberman <brian at innovationslab.net>
* Full e-mail: http://www.ietf.org/mail-archive/web/6tisch/current/msg03133.html

> I am not aware of a BCP that discusses references to external documents.
>  Format-wise, that is the purview of the RFC Editor.  I am not sure why
> the IAB would care about references...
> 
> I am generally leery of a blanket reference to the generic
> specification.  The WG has developed its solution based on a particular
> version of the IEEE specification, so I would expect to see references
> annotated by the year of the 15.4 spec.

-----

* position: **DISCUSSION**
* Date: Tue, 31 Mar 2015 12:57:38 +0000
* From: "Pascal Thubert (pthubert)" <pthubert at cisco.com>
* Full e-mail: http://www.ietf.org/mail-archive/web/6tisch/current/msg03134.html

> Adding the year is apparently not the expectation of the group that owns the reference that we make. I mentioned the IAB because of a best practice question; I'm asking if there needs to be a best practice at the IETF like there is at IEC as well-documented in Tom Phinney's email, and if so, then who should write it? I take from your email that you do not think there is such a need.
> 
> Regardless, there appears to be a tension that 6TiSCH needs to resolve, I really like the way IEC did it, and I would not mind doing the exact same: If you have a dependency on a text that is particular to one version, be specific, else be generic.
> 
> So far, 6TiSCH does not have a dependency on a particular version, so we could keep it generic with 802.15.4, with a mention that a part of the overall interoperability depends on the MAC and PHY layers and is independent on 6TiSCH.
> 
> My reading is that we'd leave it up to manufacturers to select the MAC/PHY version that they like, and they have their own reasons to pick one; the caveat is that they might not interop with one another at the IEEE layers, but should that be our problem?
> 
> At least I understand that we need to be specific for the plug test. And that we can resolve within the WG.

-----

* position: **AGAINST**
* Date: Tue, 31 Mar 2015 09:52:09 -0400
* From: Rene Struik <rstruik.ext at gmail.com>
* Full e-mail: http://www.ietf.org/mail-archive/web/6tisch/current/msg03135.html

> The 802.15.4rev draft is, as the wording suggests, a *draft* specification. One should not give the impression as if this would be a stable draft, let alone a specification cast in stone. Thus, referencing to this as "802.15.4-2015" puts people on the wrong footing. Moreover, already anticipating that this draft reaches the end station in 2015, even prior to start of sponsor ballot, casts some doubt as to how receptive one would be to consider comments received during sponsor ballot.
> 
> My only comment right now is that, in its current form, removing the "e" is premature. This may change over time, but one should not let 802.15 folks be the arbiter of this, but users of this specification, including 6TiSCH.
> 
> BTW - ZigBee mostly uses the 802.15.4-2006 specification and most 6lowpan documents also refer to that specification.

-----

* position: **DISCUSSION**
* Date: Tue, 31 Mar 2015 14:18:23 +0000
Cc: "Brett, Patricia \(PA62\)" <patricia.brett at honeywell.com>
* Full e-mail: http://www.ietf.org/mail-archive/web/6tisch/current/msg03137.html

> Is there any middle ground?
> Could there be a  reference 802.15.4e, noting it is a draft which is to be finalized and included in the 802.15.4-2015 standard?

-----

* position: **DISCUSSION**
* Date: Tue, 31 Mar 2015 07:30:00 -0700
* From: Tom Phinney <tom.phinney at cox.net>
* Full e-mail: http://www.ietf.org/mail-archive/web/6tisch/current/msg03139.html

> Just to be clear about the ISO/IEC approach, there are two places where a dated reference might be specified. The first is simply within the Normative References clause of a standard, where the specific edition to which the standard refers can be cited when that is deemed appropriate. For IETF documents, where a different document number is assigned for each revised republication, the document number alone suffices. However, for standards from other standards development organizations (SDOs), the specific year of the edition usually is required when it is deemed necessary to uniquely identify a specific edition of the cited standard.
> 
> The second place where a dated reference must be specified is when the text of the standard cites a specific subclause of the referenced other standard as normative text. In that case it is necessary to cite the year of the referenced standard within the reference so that an oversight during future editing of the citing standard does not inadvertently reference the wrong clause of a revision of the referenced standard, which could otherwise occur when the base edition of the referenced standard is updated within the Normative References clause of the citing standard.
> 
> A hypothetical example from the current documents shows the various difficulties:
> 
> - Current normative references within 6tich documents would be to IEEE 802.15.4:2011 or to IEEE 802.15.4e:2012. Both would be listed in the document's Normative References clause.
> 
> - A textual reference could take the generic form "IEEE 802.15.4", which would be appropriate when referring to aspects that are common among all editions and revisions to IEEE 802.15.4.
> 
> - A second form of textual reference would be to either "IEEE 802.15.4:2011" or "IEEE 802.15.4e:2012", which designates the specific referenced source document but not a specific subclause within that cited document.
> 
> - The third form of textual reference is to a specific subclause of a specific document. In that case the reference form must include the date, such as "IEEE 802.15.4e:2012, 4.7.2" to reference subclause 4.7.2 of IEEE 802.15.4e:2012. Including the date at the point of reference ensures detection of future editing errors during maintenance of the citing standard.
> 
> Note that the specific syntax of an undated or dated reference depends on the SDO that is writing the standard. The above shows the required syntax for ISO/IEC standards, in which the document number itself often contains a hyphen ("-"), thus making the hyphen unusable as a date separator. IETF and IEEE each have somewhat different requirements. The rules about inclusion of spaces within the citation also differ by SDO.

-----

* position: **AGAINST**
* Date: Tue, 31 Mar 2015 10:01:36 -0700
* From: Jonathan Simon <jsimon at linear.com>
* Full e-mail: http://www.ietf.org/mail-archive/web/6tisch/current/msg03145.html

> I agree with Rene, “802.15.4-2011 as amended by 802.15.4e-2012” is a perfectly valid specification, and one that has formed the basis of a number of products, including my company’s.
> 
> I believe that Pat is trying to faithfully represent upcoming changes, and that the primary goal of 802.15.4-2015 effort is to correct mistakes and remove ambiguities that might result in different implementations.  Yet there have been a few incompatible changes introduced, perhaps for valid reasons - I would argue the short address doesn’t leak info if only authenticating, and doesn’t prevent the combination of EUI + ASN + Key from being replayed, so that more moderate informative language could be used to solve this particular problem.  I also think that policing IE’s is naive - the IEEE doesn’t own the band, and a robust design uses security to ensure that frame content is correct. 
> 
> So I guess it comes down to two things:
> 
> 1) are the incompatible changes adding something that is needed for 6TiSCH - if not, we are free in 6TiSCH to override behavior we don’t like, e.g. “x is as described in spec y, except for the limitation of clause z, which is taken from spec p, clause q”
> 2) do we need to see 802.15.4-2015 before adopting it as a normative reference?  Paranoid me says yes, but if people agree to 1) then its not really a problem.

-----

* position: **AGAINST**
* Date: Tue, 31 Mar 2015 13:19:54 -0400
* From: Subir Das <subirdas21 at gmail.com>
* Full e-mail: http://www.ietf.org/mail-archive/web/6tisch/current/msg03147.html

> Agree with what Brian said here. I think 802.15.4- 2011 is more appropriate unless 802.15.4 - 2015 is published  by the IEEE-SA before the RFC is published. If we choose to do 802.15.4-2015, then it would be good if WG discusses and understands the impact to IETF document(s), if there is any.

-----

* position: **AGAINST**
* Date: Tue, 31 Mar 2015 13:20:33 -0400
* From: Brian Haberman <brian at innovationslab.net>
* Full e-mail: http://www.ietf.org/mail-archive/web/6tisch/current/msg03148.html

> If we were to assume that:
> 
> 1. IEEE publishes 802.15.4-2015
> 2. the 6tisch protocol spec is published *after* 802.15.4-2015
> 3. the published 6tisch protocol references generically 802.15.4
> 
> what happens to the IETF spec if the IEEE publishes 802.15.4-2017, that
> spec overhauls the 15.4e behavior, and renders the 6tisch spec useless?
> 
> To me, the above would argue that referencing 802.15.4-2015 makes it
> clear which version of that spec is needed to support the 6tisch spec.
> 
> The above may not be an issue given I am still coming up to speed on all
> aspects of 6tisch.

-----

* position: **AGAINST**
* Date: Tue, 31 Mar 2015 11:20:03 -0700
* From: Kris Pister <ksjp at berkeley.edu>
* Full e-mail: http://www.ietf.org/mail-archive/web/6tisch/current/msg03150.html

> A valid concern.  802.15.4e:2012 seems like the safest reference for the
> minimal draft at the very least.

-----

* position: **IN_FAVOR**
* Date: Wed, 1 Apr 2015 09:22:21 +0000
* From: "Pascal Thubert (pthubert)" <pthubert at cisco.com>
* Full e-mail: http://www.ietf.org/mail-archive/web/6tisch/current/msg03156.html

> Hi Jonathan, René and all:
> 
> But the point is, we are not working in silos like industrial protocols, defining the whole stack from PHY to APP. We are defining an IETF stack, which happens to be designed to apply above the TSCH mode of in 802.15.4 that was introduced by 802.15.4e.
> 
> Because we work on layers as opposed to silos, the 6TiSCH logo guarantees interop at the IP layer, not at the MAC or PHY layers.
> 
> E.g. IP (as defined over Ethernet) works on all Wi-Fi versions. But I can hardly connect my old 802.11a card to any network these days. Sad it is, but that is not an IETF problem.
> 
> In the hypothetical case that IEEE802.15.4:2015 is not compatible with  “802.15.4-2011 as amended by 802.15.4e-2012”, and that 6TiSCH, as specified, works on both without a change, then we should still refer to IEEE802.15.4.
> 
> MAC compatibility problems are an IEEE problem, not an IETF problem; there has also been a concern in this thread that the changes on MAC/PHY require too much new work. All this is certainly unfortunate, but it has to be resolved at the IEEE, not on this list.  We have an Interest Group and may leverage it, but still this list is not the right forum and the discussion is taking us way beyond the question I asked to the group.
> 
> And because silos are still needed to guarantee interoperability, products that support 6TiSCH will have to bear some logo that indicate what MAC/PHY they use, as well as which security models they support; there will be multiple such logos that bind all that together and guarantee some interoperability and functionality. IOW, it is up to an adopting standard (a ZigbeeIP, an ISA, an IEC, a Thread or an HCF of this world) to be more specific of the whole stack they build upon, and the IETF does not need to have a say in that.
> 
> René’s suggestion, “802.15.4-2011 as amended by 802.15.4e-2012”, would for instance fail to include 802.15.4g. There are shipping products that do TSCH over the sub-Gig in AMI/AMR and SmartGrid use cases, and barring them from moving to 6TiSCH makes no sense. 6TiSCH is probably a great solution for WiNAN. So I am strongly opposed to that limiting wording. From the IEEE perspective, “IEEE802.15.4” *today* means “802.15.4-2011 as amended by 802.15.4e-2012, 802.15.4f-2012,  802.15.4g, 802.15.4j, 802.15.4k, 802.15.4m, and 802.15.4p". For all I know that is *exactly* what we want.
> 
> There has also been concerns that 6TiSCH RFCs may fail to work as expected over some future version of 802.15.4. This may become true, and this may be plain FUD. But yes, that is our problem. I’ll be asking around if there is a best practice between IEEE and IETF for such thing, above and beyond our particular case. My current research indicates that we have been widely inconsistent so far. One suggestion could be to place a note in the reference, as discussed in RFC4897 though for other purposes. This note could indicate that “this RFC applies to the version of 802.15.4 that is current at the time of publication of this RFC, and that it is expected but not guaranteed that this RFC applies to 802.15.4 versions published after the publication of this RFC”.
> 
> So the updated action plan that I’m proposing to the group is as follows:
> - still, remove the ‘e’ in the charter; additionally:
> - work on a note for the reference to address the questions raised about compatibility with future versions.
> - lookup for a best practice in that area (I’m taking that action item but help is welcome)
> - task the IEEE 6TiSCH interest group to check whether the current text impacts the 6TiSCH works as it stands (including drafts discussed for re-chartering such as 6top and OTF)
> 
> Does this make sense?

-----

* position: **IN_FAVOR**
* Date: Wed, 1 Apr 2015 12:24:04 +0000
* From: "Pascal Thubert (pthubert)" <pthubert at cisco.com>
* Full e-mail: http://www.ietf.org/mail-archive/web/6tisch/current/msg03160.html

> Hello Brian:
> 
> > To me, the above would argue that referencing 802.15.4-2015 makes it clear which version of that spec is needed to support the 6tisch spec.
> 
> Whatever we do, we want to make sure that we do not lock 6TiSCH to a particular version of IEEE standards in such a fashion that complying products are stuck to that version.
> 
> The general expectation is that anything that depends on an IEEE standard works the same on the newer version of that standard that gets published. So we are supposed to reference the standard not the year. But as you point out this leave a lot in the vague. Including the starting point from which the reference applies, which for us means the day 802.15.4e was published, because implicitly 4e was included in the 802.15.4 reference from that day on.
> 
> Even if the MAC becomes incompatible with earlier versions, or PHYs are added or deprecated, history shows that the general expectation above tends to stand true, and the interop problem, if any, generally occur at the MAC/PHY layers if at all.  For instance, the IEEE was free to upgrade, say, Ethernet bridging from spanning tree to L2R, and that was transparent to IP as should be. Yes, IP has dependencies on Ethernet, but they are loose enough to allow transparent upgrades on both layers. 
> 
> I have been reviewing how the IETF has proceeded in the past, and found that by leaving it to the WGs we have been quite inconsistent:
> 
> - RFC894 has NO reference to Ethernet at all, indicates that the "memo applies to the Ethernet (10-megabit/second, 48-bit addresses)", but does not limit the applicability to other speeds. 
> Clearly the memo was implicitly extended to any subsequent version of Ethernet.
> 
> - RFC 2644 has NO reference to Ethernet either, but just a reference to a EUI-64 tuto, and that was before a best practice was established to separate normative and informative reference in RFCs.
> 
> - RFC 4944 has the following NORMATIVE reference:
> 	[ieee802.15.4]  IEEE Computer Society, "IEEE Std. 802.15.4-2003", October 2003.
> Does it mean that the 6LoWPAN mesh header is not defined on the 2006 and the 2011 versions of 802.15.4?
> 
> - RFC 6282 points on the 2006 version, but as INFORMATIVE:
> 	[IEEE802.15.4]  IEEE Computer Society, "IEEE Std. 802.15.4-2006", October 2006.
> Does it mean that we cannot compress IPv6 on the 2011 version?
> 
> - https://tools.ietf.org/html/draft-rfc-editor-rfc2223bis is mostly a formatting guide but does not enter such consideration.
> 
> - BCP 97 and RFC4897 do not discuss documents that are sourced outside the IETF; they are about procedures and warning that a down-ref exists and when that can be acceptable.
> 
> All in all, we have ignored the problem, been quite creative, and mostly placed ourselves in some twilight zone if not a corner; there is a need clarify what our position should be; and I do not see that the RFC editor should be responsible of producing that general practice. I understand that this is out of scope for the IAB. 
> 
> I'll be asking around, e.g. taking the generic question to IEEE-IETF coordination see if there is a position that we are unaware of. So far my take is that we should reference the generic standard with a carefully written note that explain from which version of the standard we start to operate, and that we expect but cannot guarantee compatibility with versions of the standard published after the date of the RFC.
> 
> Makes sense?

-----

* position: **AGAINST**
* Date: Wed, 01 Apr 2015 10:34:36 -0400
* From: Rene Struik <rstruik.ext at gmail.com>
* Full e-mail: http://www.ietf.org/mail-archive/web/6tisch/current/msg03161.html

> My assessment was (and is) that it is imprudent to remove the "e" at this point in time. I never suggested that, *after* doing proper due diligence we could still move along that route.
> 
> Your message below seems more a branding exercise, which should be completely tangential to the question you asked to the 6TiSCH community on the mailing list.
> 
> All the goodies and rosy things on the horizon you sketched below may still materialize. The only point right now is that the "scrapping the e act" is premature.
> 
> BTW - MAC compatibility problems may very well impact IETF, in protocols whose design may make assumptions on MAC properties and on, e.g., 6top interfaces. If 6TiSCH would be indeed be completely independent of the underlying MAC ("MAC compatibility problems are an IEEE problem, not an IETF problem"), then why have all the 6TiSCH documents that have MAC language in them? 

-----

* position: **IN_FAVOR**
* Date: Wed, 1 Apr 2015 16:53:15 +0000
* From: "Pascal Thubert (pthubert)" <pthubert at cisco.com>
* Full e-mail: http://www.ietf.org/mail-archive/web/6tisch/current/msg03169.html

> Removing the ‘e’ *today* is effectively transparent with regards 802.15.4e and 802.15.4-2011 because today that’s what a reference to 802.15.4 means. How is that imprudent?
> 
> And no I do not see that what I’m saying in not precisely tangential. Can you explain yourself?
> 
> From my standpoint, I have re-centered the discussion to my original question, and added clarifying questions/suggestions.
> 
> Original question:
> - can we remove the ‘e’ in the charter  
> clarifying questions:
> - suggestion to place in a note for the reference to address the questions raised about compatibility with future versions, asking wording.
> - on best practice in IEEE references (this is being resolved at this very moment with the IEEE coordination and the RFC editor)
> - and to the IEEE 6TiSCH interest group and whoever has access to the IEEE draft version: whether and how the current text impacts the 6TiSCH work as it stands.
> 
> What is tangential and digressive is references to MAC layer changes that do not affect L3 (or 6top). What goal is that serving?
> 
> And certainly, we will be interested in changes that would impact the 6top interface so we can add the required stuff to the data model.
> 
> If issues are identified, we can probably hold 6top interface till the IEEE document is ratified and published.

-----

* position: **IN_FAVOR**
* Date: * From: Pat Kinney <pat.kinney at kinneyconsultingllc.com>
* From: * Date: Wed, 1 Apr 2015 12:44:37 -0500
* Full e-mail: http://www.ietf.org/mail-archive/web/6tisch/current/msg03170.html

> Pascal is correct in his opening statement, at the present the term "802.15.4" actually means: 
> 
> "802.15.4-2011 as amended by 802.15.4e-2012, 802.15.4f-2012, 802.15.4g, 802.15.4j, 802.15.4k, 802.15.4m, and 802.15.4p"
> 
> So dropping the use of 802.15.4e and using 802.15.4 does not change the charter of 6tisch (the other amendments add additional PHYs to the standard).  When the revision is approved, the term “802.15.4” will refer to “802.15.4-2015” assuming the approval is done this year.
> 
> As Tero has been pointing out to the 6tisch WG (and others to the 802.15 WG), the security in 802.15.4-2011 is broken, i.e. compliance to 802.15.4-2011 as amended by 802.15.4e-2012 ensures that the security will not properly function. For example, there was the issue with the short address alignment ambiguity for the TSCH, 802.15.4-2011 had problem with unsecured frames, as it did keydescriptor lookup and that failed for those unsecured keys (i.e. compliance with the procedure  in 7.2.3 results in rejecting unsecured frames).  The implementations to which I am aware work around these ambiguities in proprietary manners.
> 
> For the above issues and many others, I do not believe that “compliance" to 802.15.4-2011 as amended by 802.15.4e-2012 is critical.  
> 
> As I read these emails, I am lead to believe that most are concerned with the “compatibility” of code as per the revision with today’s code (K Pister described his concern that the revison will require a re-write of existing code).  To this point I must reply that existing code with vendor specific IEs could require code change since the revision requires vendor specific IEs to start off with the vendor’s OUI.  However, I note that the 6tisch effort to date does not describe the use of vendor specific IEs. To the point of the unsecured frame issue, if existing code repairs the problem in a similar fashion to the revision, no change should be required.  As to the issue of using the short address to construct the nonce, I am open to hearing arguments for and against this practice.
> 
> Once again, I must restate my request that all technical issues and concerns be specific and made to this ML.  This will allow us to address technical issues not fear, uncertainty, and doubt (FUD).

-----

* position: **IN_FAVOR**
* Date: Fri, 3 Apr 2015 01:36:38 +0300
* From: Tero Kivinen <kivinen at iki.fi>
* Full e-mail: http://www.ietf.org/mail-archive/web/6tisch/current/msg03184.html

> Ps. I have been too busy to really follow the 6tisch mailing list, but
> perhaps I need to find time to do it. I did read the "removing the 'e'
> thread" in the archives, and I think the correct thing would be to
> refer to the 802.15.4 without any year or specific list of amendments. 
