# Monday

| status            | presenter                 | MeetEcho | filename                                                         |
|-------------------|---------------------------|----------|------------------------------------------------------------------|
| OK                | Chairs                    | NO       | `ietf92a_00_intro-status.ppt`                                    |
| OK                | Norman Finn               | NO       | `ietf92a_01_draft-finn-detnet-architecture-00.ppt`               |
| OK                | Jouni Korhonen            | NO       | `ietf92a_02_draft-gunther-detnet-proaudio-req-00.ppt`            |
| OK                | Patrick Wetterwald        | NO       | `ietf92a_03_draft-wetterwald-detnet-utilities-reqs-01.pptx`      |
| OK                | Chonggang Wang            | NO       | `ietf92a_04_draft-wang-6tisch-track-use-cases-00.ppt`            |
| **MISSING!!**     | Michael Richardson        | NO       | `ietf92a_05_dt-status.ppt`                                       |
| OK                | Rene Struik               | NO       | `ietf92a_06_draft-struik-6tisch-security-considerations-01.ppt`  |
| OK                | Pascal Thubert            | NO       | `ietf92a_07_wrap-up.ppt`                                         |

# Thursday

| status            | presenter                 | MeetEcho | filename                                                         |
|-------------------|---------------------------|----------|------------------------------------------------------------------|
| **UPDATE NEEDED** | Chairs                    | NO       | `ietf92b_00_intro-status.ppt`                                    |
| OK                | Thomas Watteyne           | NO       | `ietf92b_01_draft-ietf-6tisch-tsch-06.ppt`                       |
| OK                | Nicola Accettura          | NO       | `ietf92b_02_draft-ietf-6tisch-minimal-06.ppt`                    |
| OK                | Pascal Thubert            | NO       | `ietf92b_03_draft-ietf-6tisch-architecture-06.ppt`               |
| OK                | Maria Rita Palattella     | **YES**  | `ietf92b_04_draft-ietf-6tisch-terminology.ppt`                   |
| OK                | Xavi Vilajosana           | **YES**  | `ietf92b_05_draft-ietf-6tisch-6top.ppt`                          |
| OK                | Xavi Vilajosana           | **YES**  | `ietf92b_06_draft-ietf-6tisch-coap-03.pptx`                      |
| OK                | Miguel Angel Reina Ortega | **YES**  | `ietf92b_07_plugtest.ppt`                                        |
| OK                | Diego Dujovne             | NO       | `ietf92b_08_draft-dujovne-6tisch-on-the-fly-05.ppt`              |
| OK                | Chairs                    | NO       | `ietf92b_09_rechartering.ppt`                                    |
