   6tisch-security weekly meeting
   present:  Malisa, Savio, Francesca, Goran, Michael,
   agenda:
   [1]https://www.ietf.org/mail-archive/web/6tisch-security/current/msg005
   44.html
   - start:
       pairwise PSKs
   - end:
       K2  start state:
       - each mote is pre-configured with a key, the same key is written
   in the JCE together with its MAC address  end state:
       - the JCE has authenticated the mote; the mote has authenticated
   the JCE
       - the JCE has installed (through secure session with the JN) key
   K2. This key is then used for link-layer AUTH+ENV CCM*
       - the PSK enables a session between the node and the JCE. The JCE
   uses that session to send commands to the mote to (1) rotate K2, (2)
   change PSK
   [2]http://www.sandelman.ca/tmp/6tisch-security-26.pdf  <- slides from
   Goeran.

References

   1. https://www.ietf.org/mail-archive/web/6tisch-security/current/msg00544.html
   2. http://www.sandelman.ca/tmp/6tisch-security-26.pdf
